#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{

    // Hash the guess using MD5
    char* g =md5(guess,strlen(guess));

    
    // Compare the two hashes
    int valid = 0;
    if(strcmp(hash,g)==0){
        valid=1;
        printf("%s\n",guess);
    }
    // Free any malloc'd memory
    free(g);
    
    return valid;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    //mallock space for the entire file
    struct stat st;
    
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    
    char *file=malloc(len);
    
    //read entire file into memory
    FILE *f = fopen(filename,"r");
    if(!f){
        printf("cant open %s for read\n",filename);
        exit(1);
    } 
    fread(file,1,len,f);
    
    //replace \n with \o
    int count=0;
    for(int i=0;i<len;i++){
        if(file[i]=='\n'){
             file[i]='\0'; 
             count++;
        }
    }
    
    //mallock space for character pointers
    char ** line=malloc((count+1)*sizeof(char*));
    
    //fill in the address
    int word=0;
    line [word]= file; //the first word in the file'
    word++;
    
    for(int i=0;i<len;i++){
        if(file[i]=='\0'&&i+1<len){
            line[word]=&file[i+1];
            word++;
        }
        
    }
    
    line[word]=NULL;
    
    //return address of second array
    //*size=word-1;
    return line;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
     FILE *f = fopen("output.txt", "w");
     if (!f)
        {
            printf("Can't open %s for reading\n", argv[1]);
        }
        
    int i = 0;
    int valid=0;
    while(hashes[i]!=NULL)
    {
        
        int d=0;
        while(valid==0){
            valid=tryguess(hashes[i],dict[d]);
            d++;
        }
        //printf("%s\n",hashes[i]);
        fprintf(f,"%s\n",hashes[i]);
        i++;
        valid=0;
    }
    
    fclose(f);
}
